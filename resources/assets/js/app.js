import Vue from 'vue';
import "core-js/fn/object/assign";
import App from '../components/App.vue';
import ListingPage from '../components/ListingPage.vue';
import router from './router';
import store from './store';


//Vue.component('image-carousel',
//        {
//            template:
//                    `<div class="image-carousel">  
//           <img v-bind:src="image"/> 
//
//             <div class="controls">
//                <carousel-control dir="left" @change-image="changeImage"></carousel-control>
//                <carousel-control dir="right" @change-image="changeImage"></carousel-control>
//              </div>
//            </div>`
//            ,
//            props: ['images'],
//            data() {
//                return {index: 0}
//            },
//            methods:{
//                changeImage(val){
//                    let newVal=this.index + parseInt(val) ;
//                    if (newVal < 0){
//                        this.index=this.images.length-1;
//                    }
//                    else if(newVal === this.image.length){
//                        this.index=0;
//                    }
//                    else{
//                        this.index=newVal;
//                    }
//                    
//                }
//            },
//            computed: {
//                image() {
//                    return this.images[this.index];
//                },
//
//            },
//            components: {
//                'carousel-control': {
//                    template: `<i :class="classes" @click="clicked"></i>`,
//                    props: ['dir'],
//                    computed: {
//                        classes() {
//                            return 'carousel-control fa fa-2x fa-chevron-' + this.dir
//                        }
//                    },
//                    methods: {
//                        clicked() {
//                            this.$emit('change-image',this.dir === 'left' ? -1 : 1)
//                        }
//                    },
//                
//                }
//            }
//        });

//import sample from './data';
//let model = JSON.parse(window.vuebnb_listing_model);

var app = new Vue({
    el: "#app",
//    render: h => h(ListingPage),
render: h => h(App),
    router,
    store
//    data: Object.assign(model, {
//        // title:"My Apartment",
//        // address:"12 My Street, My city, My country",
//        // about:"This is description of my apartment"
//        title: model.title,
//        address: model.address,
//        about: model.about,
//        headerImageStyle: {
//            'background-image': `url(${model.images[0]})`
//        },
//        amenities: model.amenities,
//        prices: model.prices,
//        contracted: true,
//
//        message: 'Hello world'
//    }),
//   
//    //Hook life Cycle
//    beforeCreate: function () {
//        console.log('beforeCreate message: ' + this.message);
//        // "beforeCreate message: undefined"
//    },

});
setTimeout(function () {
    app.message = 'Goodbye world'
}, 2000);
document.addEventListener('keyup', function (evt) {
    if (evt.keyCode === 27 && app.modalOpen) {
        app.modalOpen = false;
    }
});