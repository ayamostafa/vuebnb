/* 
 * Created by Aya Mostafa.
 * Date: Nov 27, 2018
 */


   <div class="lists">
                    <hr>
                    <div class="amenities list">
                        <div class="title"><strong>Amenities</strong></div>
                        <div class="content">
                            <div class="list-item" v-for="amenity in amenities">
                                <i class="fa fa-lg" v-bind:class="amenity.icon"></i>
                                <span>@{{ amenity.title }}</span> </div>
                        </div>
                    </div>
                    <hr>
                    <div class="prices list">
                        <div class="title"> <strong>Prices</strong> </div>
                        <div class="content">
                          <div class="list-item" v-for="price in prices">
            @{{ price.title }}: <strong>@{{ price.value }}</strong>
          </div>

                        </div>
                    </div>
                </div>